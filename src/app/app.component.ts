import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  photos = [
    {
      url: 'https://http2.mlstatic.com/kit-com-sementes-de-ip-D_NQ_NP_104905-MLB25084635190_102016-F.jpg',
      descricao: 'ipê Vermelho'
    },
    {
      url: 'https://sementescaicara.bbshop.com.br/content/images/thumbs/0003314_ipe-amarelo-cascudo.jpeg',
      descricao: 'Ipê Amarelo cascudo'
    },
    {
      url: 'https://www.soflor.com.br/wp-content/uploads/2017/12/ipe-roxo-sementes.jpg',
      descricao: 'Ipê Roxo'
    }
  ];

 }
