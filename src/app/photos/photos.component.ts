import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.css']
})
export class PhotosComponent implements OnInit {

  // // titulo = 'Galeria de imagens';
  // // url="https://images.pexels.com/photos/36764/marguerite-daisy-beautiful-beauty.jpg?auto=compress&cs=tinysrgb&h=750&w=1260";
  // // descricao = 'Girasol';
  @Input() url = '';
  @Input() descricao = '';
 
    constructor() { }

  ngOnInit() {
  }

}
